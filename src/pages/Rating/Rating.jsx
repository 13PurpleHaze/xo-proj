import React, {useState} from 'react';
import Table from "./Table";
import classes from "./rating.module.css";
import Timer from "../../components/Timer/Timer";

const Rating = () => {
    const [players, setPlayers] = useState([
        {
            id: 1,
            fio: "Александров Игнат Анатольевич",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "55%",
        },
        {
            id: 2,
            fio: "Шевченко Рафаил Михаилович",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "55%",
        },
        {
            id: 3,
            fio: "Мазайло Трофим Артемович",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "55%",
        },
        {
            id: 4,
            fio: "Логинов Остин Данилович",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "55%",
        },
        {
            id: 5,
            fio: "Борисов Йошка Васильевич",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "55%",
        },
        {
            id: 6,
            fio: "Соловьев Ждан Михаилович",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "55%",
        },
        {
            id: 7,
            fio: "Негода Михаил Эдуардович",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "55%",
        },
        {
            id: 8,
            fio: "Гордеев Шамиль Давидович",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "55%",
        },
        {
            id: 9,
            fio: "Многогрешный Павел Витальевич",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "55%",
        },
        {
            id: 10,
            fio: "Александров Игнат Анатольевич",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "55%",
        },
        {
            id: 11,
            fio: "Волков Эрик Алексеевич",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "55%",
        },
        {
            id: 12,
            fio: "Кузьмин Ростислав Владимирович",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "55%",
        },
        {
            id: 13,
            fio: "Стрелков Филип Борисович",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "55%",
        },
        {
            id: 14,
            fio: "ГАлкин Феликс Платонович",
            totalGames: 24534,
            wins: 9824,
            loses: 1222,
            winsPercent: "87%",
        },
    ])
    return (
        <div className={classes.rating}>
            <h1 className={classes.rating__title}>Рейтинг игроков</h1>
            <Table players={players}/>
        </div>
    );
};

export default Rating;