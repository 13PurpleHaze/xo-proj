import React from 'react';
import classes from "./rating.module.css";

const Table = ({players}) => {
    return (
        <div className={classes.wrapper}>
            <table>
                <thead>
                <tr className={classes.table__row}>
                    <th className={classes.table__cell}>ФИО</th>
                    <th className={classes.table__cell}>Всего игр</th>
                    <th className={classes.table__cell}>Победы</th>
                    <th className={classes.table__cell}>Проигрыши</th>
                    <th className={classes.table__cell}>Процент побед</th>
                </tr>
                </thead>
                <tbody>
                {players.map(player =>
                    <tr key={player.id} className={classes.table__row}>
                        <td className={`${classes.table__cell} ${classes.rating__fio}`}>{player.fio}</td>
                        <td className={classes.table__cell}>{player.totalGames}</td>
                        <td className={`${classes.table__cell} ${classes.wins}`}>{player.wins}</td>
                        <td className={`${classes.table__cell} ${classes.loses}`}>{player.loses}</td>
                        <td className={classes.table__cell}>{Math.round(player.wins / player.totalGames * 100)}%</td>
                    </tr>
                )}
                </tbody>
            </table>
        </div>
    );
};

export default Table;