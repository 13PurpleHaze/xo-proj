import React, {useMemo, useState} from 'react';
import Input from "../../components/ui/input/Input";
import SubmitButton from "../../components/ui/button/SubmitButton";
import classes from './login.module.css';
import {useNavigate} from "react-router-dom";

const Login = () => {
    const [user, setUser] = useState({
        login: '',
        password: '',
    }) 
    const [errorLogin, setErrorLogin] = useState('')
    const [errorPassword, setErrorPassword] = useState('')
    const [isDisabled, setIsDisabled] = useState(false)

    const navigate = useNavigate();
    const validate = (value) => {
        const regex = /^[0-9A-Za-z._]+$|^$/;
        return regex.test(value)
    }

    const validLogin = (e) => {
        console.log(e.target.value)
        if (!validate(e.target.value)) {
            e.preventDefault();
        } else {
            setUser({...user, login: e.target.value})
        }
    }
    const validPassword = (e) => {
        if (!validate(e.target.value)) {
            e.preventDefault();
        } else {
            setUser({...user, password: e.target.value})
        }
    }

    useMemo(() => {
        setIsDisabled(user.login.length > 0 && user.password.length > 0);
    },[user]);


    return (
        <div className={classes.wrapper}>
            <form className={classes.form} method="post">
                <div className={classes.form__img}>
                    <img src={"/img/dog.png"} alt="" className={classes.img}/>
                </div>
                <h1 className={classes.form__title}>Войдите в игру</h1>
                <div className={classes.form__body}>
                    <div className={classes.input__group}>
                        <Input type="text" placeholder="Логин" error={errorLogin} value={user.login}
                               onChange={(e) => validLogin(e)}/>
                        {
                            errorLogin ? <span className={classes['error-message']}>{errorLogin}</span> : <></>
                        }
                    </div>
                    <div className={classes.input__group}>
                        <Input type="password" placeholder="Пароль" error={errorPassword} value={user.password}
                               onChange={(e) => validPassword(e)}/>
                        {
                            errorPassword ? <span className={classes['error-message']}>{errorPassword}</span> : <></>
                        }
                    </div>
                </div>
                <SubmitButton type="submit" isDisabled={!isDisabled} disabled={!isDisabled} onClick={() => {navigate("/rating")}}>Войти</SubmitButton>
            </form>
        </div>
    );
};

export default Login;