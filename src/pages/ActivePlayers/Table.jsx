import React from 'react';
import classes from "./active-players.module.css";
import GeneralButton from "../../components/ui/button/GeneralButton";
import FreeStatus from "../../components/ui/status/FreeStatus";

const Table = ({players}) => {
    return (
        <div>
            {players.map(player =>
                <div key={player.id} className={classes.table__row}>
                    <div className={`${classes.table__cell} ${classes.player__fio}`}>{player.fio}</div>
                    <div className={classes.table__cell}><FreeStatus status={player.status}/></div>
                    <div className={classes.table__cell}><GeneralButton status={player.status} disabled={!player.status}>Позвать игрока</GeneralButton></div>
                </div>
            )}
        </div>
    );
};

export default Table;