import React, {useMemo, useState} from 'react';
import classes from "./active-players.module.css";
import Toggle from "../../components/ui/toggle/Toggle"
import Table from "./Table";

const ActivePlayers = () => {
    const [players, setPlayers] = useState([
        {
            id: 1,
            fio: "Александров Игнат Анатольиевич",
            status: true,
        },
        {
            id: 2,
            fio: "Василенко Эрик Платонович",
            status: false,
        },
        {
            id: 3,
            fio: "Быков Юрий Витальевич",
            status: true,
        },
        {
            id: 4,
            fio: "Галкин Феликс Платонович",
            status: false,
        },
        {
            id: 5,
            fio: "Шевченко Рафаил Михаилович",
            status: false,
        },
        {
            id: 6,
            fio: "Гордеев Шамиль Леонидович",
            status: false,
        },
        {
            id: 7,
            fio: "Бобров Фёдор Викторович",
            status: true,
        },
        {
            id: 8,
            fio: "Гордеев Шамиль Леонидович",
            status: true,
        },
        {
            id: 9,
            fio: "Суфоров Феликс Григориьевич",
            status: false,
        },
        {
            id: 10,
            fio: "Марков Йошка Фёдорович",
            status: true,
        },
    ])

    const [toggle, setToggle] = useState(false);

    const freePlayers = useMemo(() => {
        if(toggle) {
            return [...players].filter(player => player.status);
        }
        return players;
    }, [toggle, players]);


    return (
        <div className={classes["active-players"]}>
            <div className={classes.head}>
                <h3 className={classes.title}>Активные игроки</h3>
                <Toggle message="Только сободные" toggle={toggle} setToggle={setToggle}/>
            </div>
            <Table players={freePlayers}/>
        </div>
    );
};

export default ActivePlayers;