import React, {useState} from 'react';
import classes from "./players.module.css";
import GeneralButton from "../../components/ui/button/GeneralButton";
import Table from "./Table";
import Modal from "../../components/ui/modal/Modal";

const Players = () => {
    const [players, setPlayers] = useState([
        {
            id: 1,
            fio: "Александров Игнат Анатольевич",
            age: 24,
            gender: false,
            isBlock: true,
            dateStart: "12 октября 2021",
            dateChange: "22 октября 2021",
        },
        {
            id: 2,
            fio: "Мартынов Остап Фёдорович",
            age: 12,
            gender: false,
            status: false,
            dateStart: "12 октября 2021",
            dateChange: "22 октября 2021",
        },
        {
            id: 3,
            fio: "Комаров Цефас Александрович",
            age: 83,
            gender: true,
            status: false,
            dateStart: "12 октября 2021",
            dateChange: "22 октября 2021",
        },
        {
            id: 4,
            fio: "Кулаков Станислав Петрович",
            age: 43,
            gender: true,
            status: false,
            dateStart: "12 октября 2021",
            dateChange: "22 октября 2021",
        },
        {
            id: 5,
            fio: "Борисов Йошка Васильевич",
            age: 32,
            gender: false,
            status: false,
            dateStart: "12 октября 2021",
            dateChange: "22 октября 2021",
        },
        {
            id: 6,
            fio: "Негода Михаил Эдуардович",
            age: 33,
            gender: true,
            status: false,
            dateStart: "12 октября 2021",
            dateChange: "22 октября 2021",
        },
        {
            id: 7,
            fio: "Жданов Зураб Александрович",
            age: 24,
            gender: true,
            status: false,
            dateStart: "12 октября 2021",
            dateChange: "22 октября 2021",
        },
        {
            id: 8,
            fio: "Бобров Фёдор Викторович",
            age: 19,
            gender: true,
            status: false,
            dateStart: "12 октября 2021",
            dateChange: "22 октября 2021",
        },
        {
            id: 9,
            fio: "Александров Игнат Анатольевич",
            age: 21,
            gender: false,
            status: false,
            dateStart: "12 октября 2021",
            dateChange: "22 октября 2021",
        },
        {
            id: 10,
            fio: "Многогрешный Павел Васильевич",
            age: 24,
            gender: true,
            status: false,
            dateStart: "12 октября 2021",
            dateChange: "22 октября 2021",
        },
    ]);
    const [isShow, setIsShow] = useState(false);
    const storePlayer = (newPlayer) => {
        setPlayers([...players, newPlayer]);
    }

    return (
        <div className={classes.players}>
            <div className={classes.players__header}>
                <h3 className={classes.title}>Список игроков</h3>
                <GeneralButton status={true} action={() => {
                    setIsShow(!isShow)
                }}>Добавить игрока</GeneralButton>
            </div>
            <Table players={players} setPlayers={setPlayers}/>
            {
                isShow
                    ? <Modal setIsShow={setIsShow} storePlayer={storePlayer}/>
                    : <></>
            }
        </div>
    );
};

export default Players;