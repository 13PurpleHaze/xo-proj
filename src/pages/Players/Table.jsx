import React from 'react';
import Gender from "../../components/ui/gender/Gender";
import BlockStatus from "../../components/ui/status/BlockStatus";
import classes from "./players.module.css";
import BlockButton from "../../components/ui/button/BlockButton";

const Table = ({players, setPlayers}) => {
    const changeBlock = (player) => {
        const i = players.findIndex(pl => player.id === pl.id);
        players[i].isBlock = !player.isBlock;
        setPlayers([...players])
    }

    return (
        <div className={classes.wrapper}>
            <table>
                <thead>
                <tr className={classes.table__row}>
                    <th className={classes.table__ceil}>ФИО</th>
                    <th className={classes.table__ceil}>Возраст</th>
                    <th className={classes.table__ceil}>Пол</th>
                    <th className={classes.table__ceil}>Статус</th>
                    <th className={classes.table__ceil}>Создан</th>
                    <th colSpan={2} className={classes.table__ceil}>Изменен</th>
                </tr>
                </thead>
                <tbody>
                {players.map(player =>
                    <tr className={classes.table__row} key={player.id}>
                        <td className={`${classes.table__ceil} ${classes.ceil__fio}`}>{player.fio}</td>
                        <td className={classes.table__ceil}>{player.age}</td>
                        <td className={classes.table__ceil}>
                            <Gender gender={player.gender}/>
                        </td>
                        <td className={classes.table__ceil}>
                            <BlockStatus isBlock={player.isBlock}/>
                        </td>
                        <td className={classes.table__ceil}>{player.dateStart}</td>
                        <td className={classes.table__ceil}>{player.dateChange}</td>
                        <td className={`${classes.table__ceil} ${classes.ceil__block}`}>
                            <BlockButton isBlock={player.isBlock} action={() => {
                                changeBlock(player)
                            }}/>
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
        </div>
    );
};

export default Table;