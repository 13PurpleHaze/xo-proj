import React from 'react';
import classes from "./game-field.module.css";

const FieldItem = ({winner, nums, id, item}) => {
    return (
        <div className={(winner && nums.includes(id)) ? `${classes.cell} ${classes[winner]}` : `${classes.cell}`} id={id}>
            {
                item
                    ? <img src={`/img/xxl-${item}.svg`} alt="x"/>
                    : <></>
            }
        </div>
    );
};

export default FieldItem;