import React, {Component} from 'react';
import classes from "./game-field.module.css";
import Timer from "../../components/Timer/Timer";
import FieldItem from "./FieldItem";
import GameStep from "./GameStep";

class Field extends Component {
    constructor() {
        super();
        this.state = {
            isGameOver: false,
            winner: '',
            mode: 'x',
            field: [
                [null, null, null],
                [null, null, null],
                [null, null, null],
            ],
            nums: [], // в свойство nums по результатам игры помещяются ячейки, которые нужно закрасить
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        /*
        * каждый раз когда изменяется состояние поля, т.е. игроки ходят
        * мы изменяем текущего игрока (это нужно чтобы прокинуть потом его в чат)
        * а также проверяем победителя если игра завершилась
        * */
        const currPlayer = prevProps.players[0].mode === this.state.mode ? prevProps.players[0] : prevProps.players[1];
        this.props.setCurrPlayer(currPlayer);

        if (this.state.isGameOver) {
            this.props.setIsGameStop(true);

            if (this.state.winner === 'x') {
                const gender = prevProps.players[0].gender ? 'победил' : 'победила';
                const winner = `${prevProps.players[0].fio} ${gender}`;
                this.props.setWinner(winner);
            } else {
                if (this.state.winner === 'zero') {
                    const gender = prevProps.players[1].gender ? 'победил' : 'победила'
                    const winner = `${prevProps.players[1].fio} ${gender}`;
                    this.props.setWinner(winner);
                } else {
                    this.props.setWinner("Ничья");
                }
            }

            this.props.gameState(this);
        }
    }

    setField(x, y) {
        const f = this.state.field;
        f[x][y] = this.state.mode;
        this.setState(
            {...this.state, field: f}
        )
        const mode = this.state.mode === 'x' ? 'zero' : 'x';
        this.setState({mode: mode})
    }

    fieldCellValue(x, y) {
        if (this.state.field[x][y] === null) {
            this.setField(x, y);
            return true;
        } else {
            return false;
        }
    }

    getGameFieldStatus() {
        const area = this.state.field;
        for (const [i, row] of area.entries()) {
            /*
            * Проверка элементов по горизонтали
            * */
            let firstElement = row[0] ?? '';
            let res = row.every((item) => {
                return item === firstElement;
            })
            if (res) {
                let index = i;
                this.setState({
                    winner: this.state.mode,
                    isGameOver: true,
                    nums: [index * 3, index * 3 + 1, index * 3 + 2]
                });
                return;
            }

            /*
            * Проверка элементов по вертикали
            * */
            firstElement = area[0][i] ?? '';
            res = (area.map(row => row[i])).every((item) => {
                return item === firstElement;
            })
            if (res) {
                let index = i;
                this.setState({
                    winner: this.state.mode,
                    isGameOver: true,
                    nums: [index, index += 3, index += 3]
                });
                return;
            }
        }

        let len = area.length - 1;
        const mainDiag = area.map((row, index) => row[index]);
        const secDiag = area.map((row, index) => row[len - index]);

        /*
        * Проверка главной диагонали
        * */
        let firstElement = mainDiag[0] ?? '';
        let res = mainDiag.every((item) => {
            return item === firstElement;
        })
        if (res) {
            this.setState({winner: this.state.mode, isGameOver: true, nums: [0, 4, 8]});
            return;
        }

        /*
        * Проверка побочной диагонали
        * */
        firstElement = secDiag[0] ?? '';
        res = secDiag.every((item) => {
            return item === firstElement;
        })
        if (res) {
            this.setState({winner: this.state.mode, isGameOver: true, nums: [2, 4, 6]});
            return;
        }

        /*
        * Ничья только в том случае, когда нет null значений в массиве
        * */
        if (area.find(row => {
            return row.find(item => item === null) === null
        }) === undefined) {
            this.setState({winner: 'Ничья', isGameOver: true});
        }
    }

    /*
    * метод отслеживающий клилки по полю
    * */
    handle(e) {
        const element = e.target.closest('div');
        if (element.id && !this.state.isGameOver) {
            const x = Math.floor(element.id / 3);
            const y = element.id % 3;
            if (!this.fieldCellValue(x, y)) {
                alert('Клетка уже занята');
            } else {
                this.getGameFieldStatus();
            }
        }
    }


    render() {
        return (
            <div className={classes["game-container"]}>
                <Timer isOver={this.state.isGameOver} resetTimer={this.props.resetTimer} setResetTimer={this.props.setResetTimer}/>
                <div className={classes["game-board"]} onClick={(e) => this.handle(e)}>
                    {
                        this.state.field.map((row, i) =>
                            row.map((item, j) =>
                                <FieldItem id={j + 3 * i} key={j + 3 * i} winner={this.state.winner}
                                           nums={this.state.nums} item={item}/>
                            )
                        )}
                </div>
                <GameStep players={this.props.players} mode={this.state.mode}/>
            </div>
        );
    }
}

export default Field;