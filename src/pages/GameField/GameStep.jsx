import React, {useEffect, useState} from 'react';
import classes from "./game-field.module.css";

const GameStep = ({players, mode}) => {
    const [step, setStep] = useState('');
    useEffect(()=>{
        if(players[0].mode === mode) {
            setStep(players[0]);
        } else {
            setStep(players[1]);
        }
    },[mode])
    return (
        <div className={classes["game-step"]}>
            Ходит&nbsp;
            <img src={`/img/${step.mode}.svg`}/>
            &nbsp;{step.fio}
        </div>
    );
};

export default GameStep;