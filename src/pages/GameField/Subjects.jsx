import React, { useState } from 'react';
import classes from "./game-field.module.css";

const Subjects = ({players}) => {
    const [showMenu, setShowMenu] = useState(false);
    return (
        <div className={classes["subject-list"]}>
            <div className={classes["header-container"]}>
                <h1>Игроки</h1>
                <img src="/img/arrow-up.svg" alt="" className={ showMenu ? classes["arrow-up"] : classes["arrow-down"]} onClick={() => {setShowMenu(!showMenu)}}/>
            </div>
            <div className={showMenu ? `${classes.subjects} ${classes.active}` : classes.subjects}>
                {
                    players.map((player) =>
                        <div className={classes["subject-container"]}>
                            <div>
                                <img className={classes["subject-icon"]} src={`/img/${player.mode}.svg`}/>
                            </div>
                            <div className={classes["subject-info"]}>
                                <div className={classes["subject-name"]}>{player.fio}</div>
                                <div
                                    className={classes["subject-percent"]}>{Math.round(player.wins / player.totalGames * 100)}%
                                    побед
                                </div>
                            </div>
                        </div>
                    )
                }
            </div>
        </div>
    );
};

export default Subjects;