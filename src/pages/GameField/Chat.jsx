import React, {useEffect, useRef, useState} from 'react';
import classes from "./game-field.module.css";
import TextArea from "../../components/ui/textarea/TextArea";

const Chat = ({currPlayer, reserChat}) => {
    const [messages, setMessages] = useState([]);
    const [isOverFlowing, setIsOverFlowing] = useState(false);
    const [message, setMessage] = useState({
        player: currPlayer,
        message: '',

    });

    useEffect(() => {
        if (reserChat) {
            setMessages([]);
        }
    }, [reserChat])

    useEffect(() => {
        setMessage({...message, player: currPlayer})
    }, [currPlayer]);

    const sendMessage = () => {
        if (message.message.trim()) {
            setMessages([...messages, message]);
        }
        setMessage({...message, message: ''});
    }

    const messagesContainerRef = useRef();
    useEffect(() => {
        const messagesContainer = messagesContainerRef.current;
        const lastMessage = messagesContainer.lastElementChild;

        if (lastMessage) {
            const scrollHeight = messagesContainer.scrollHeight;
            const containerHeight = messagesContainer.clientHeight;
            const scrollOffset = scrollHeight - containerHeight;

            messagesContainer.scrollTo({ top: -scrollOffset, behavior: 'smooth' });
        }
    }, [messages]);

    return (
        <div className={classes["chat-container"]} >
            <div className={isOverFlowing ? `${classes["msgs-container"]} ${classes.isTopOverflowing} ${classes.isBottomOverflowing}` : classes["msgs-container"]} ref={messagesContainerRef} onScroll={(e) => {setIsOverFlowing(true)}}>
                {
                    messages.map((msg) =>
                        <div className={`${classes["msg-container"]} ${classes.me}`}>
                            <div className={classes["msg-header"]}>
                                <div
                                    className={`${classes["subject-name"]} ${classes[msg.player.mode]}`}>{msg.player.fio}</div>
                                <div className={classes.time}>13:41</div>
                            </div>
                            <div className={classes["msg-body"]}>{msg.message}
                            </div>
                        </div>
                    )
                }
            </div>
            <div className={classes["msg-interactive-elements"]}>
                <TextArea placeholder="Сообщение..." value={message.message}
                          onChange={(e) => setMessage({...message, message: e.target.value})}/>
                <button className={classes["button-msg"]} onClick={sendMessage}><img className={classes.btn__img}
                                                                                     src="/img/send-btn.svg"/></button>
            </div>
        </div>
    );
};

export default Chat;