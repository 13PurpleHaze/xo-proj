import React, {useEffect, useState} from 'react';
import classes from "./game-field.module.css";
import Subjects from "./Subjects";
import Field from "./Field";
import Chat from "./Chat";
import GameStopModal from "../../components/ui/modal/GameStopModal";

const GameFieldPage = () => {
    const [players, setPlayers] = useState([
        {
            id: 1,
            fio: 'Плюшкина Екатерина',
            mode: 'x',
            gender: false,
            wins: 89,
            totalGames: 103,
        },
        {
            id: 2,
            fio: 'Пупкин Владилен',
            gender: true,
            mode: 'zero',
            wins: 90,
            totalGames: 120,
        }
    ])
    const [isGameStop, setIsGameStop] = useState(false);
    const [winner, setWinner] = useState('');
    const [gameState, setGameState] = useState({})
    const [timer, setTimer] = useState(false)
    const [currPlayer, setCurrPlayer] = useState(players[0])
    const [resetChat, setResetChat] = useState(false)

    const handle = (game) => {
        /*в gameState храниться состояние игрового поля из компонента Field, это нужно чтобы ресетить игру когда она закончилась*/
        setGameState(game)
    }
    const resetGame = () => {
        gameState.setState(
            {
                isGameOver: false,
                winner: '',
                mode: 'x',
                field: [
                    [null, null, null],
                    [null, null, null],
                    [null, null, null],
                ],
                nums: [],
            }
        )
        setIsGameStop(false);
        setTimer(true);
        setResetChat(true);
    }

    return (
        <div className={classes["main-container"]}>
            <Subjects players={players}/>
            <Field players={players} setWinner={setWinner} setIsGameStop={setIsGameStop} gameState={handle}
                   resetTimer={timer} setResetTimer={setTimer} setCurrPlayer={setCurrPlayer}/>
            <Chat currPlayer={currPlayer} reserChat={resetChat}/>
            {
                isGameStop
                    ? <GameStopModal resetGame={resetGame}>{winner}</GameStopModal>
                    : null
            }
        </div>
    );
};

export default GameFieldPage;