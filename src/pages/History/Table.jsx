import React from 'react';
import classes from "./history.module.css";

const Table = ({history}) => {
    return (
        <table className={classes.table}>
            <thead>
            <tr className={classes.table__row}>
                <th className={classes.table__cell}>Игроки</th>
                <th className={classes.table__cell}>Дата</th>
                <th className={classes.table__cell}>Время игры</th>
            </tr>
            </thead>
            <tbody>
            {history.map(item =>
                <tr className={classes.table__row} key={item.id}>
                    <td className={`${classes.table__cell} ${classes.players__container}`}>
                        <div className={classes.player__one}>
                            <img src="/img/x.svg" alt="" className={classes.player__icon}/>
                            {item.playerOne}
                            {
                                item.winner === 1 &&
                                <img src="/img/cup.svg" alt="Победитель" className={classes.cup__icon}/>
                            }
                        </div>
                        <div className={classes.ceil__bold}>против</div>
                        <div className={classes.player__two}>
                            <img src="/img/zero.svg" alt="" className={classes.player__icon}/>
                            {item.playerTwo}
                            {
                                item.winner === 2 &&
                                <img src="/img/cup.svg" alt="Победитель" className={classes.cup__icon}/>
                            }
                        </div>
                    </td>
                    <td className={classes.table__cell}>{item.date}</td>
                    <td className={classes.table__cell}>{item.time}</td>
                </tr>
            )}
            </tbody>
        </table>
    );
};

export default Table;