import React, {useState} from 'react';
import classes from "./history.module.css";
import Table from "./Table";

const History = () => {
    const [history, setHistory] = useState([
        {
            id: 1,
            playerOne: "Терещенко У. Р.",
            playerTwo: "Многогрешный П. В.",
            winner: 1,
            date: "12 февраля 2022",
            time: "43 мин 13 сек",
        },
        {
            id: 2,
            playerOne: "Горбачев А.Д.",
            playerTwo: "Многогрешный П. В.",
            winner: 2,
            date: "12 февраля 2022",
            time: "43 мин 13 сек",
        },
        {
            id: 3,
            playerOne: "Константинов В. Н.",
            playerTwo: "Сасько Ц. А.",
            winner: 1,
            date: "12 февраля 2022",
            time: "43 мин 13 сек",
        },
        {
            id: 4,
            playerOne: "Никифоров Б. А.",
            playerTwo: "Горбачёв А. Д.",
            winner: 2,
            date: "12 февраля 2022",
            time: "43 мин 13 сек",
        },
        {
            id: 5,
            playerOne: "Кулишенко К. И.",
            playerTwo: "Вишняков Ч. М.",
            winner: 2,
            date: "12 февраля 2022",
            time: "43 мин 13 сек",
        },
        {
            id: 6,
            playerOne: "Гриневская Д. Б.",
            playerTwo: "Кудрявцев Э. В.",
            winner: 2,
            date: "12 февраля 2022",
            time: "43 мин 13 сек",
        },
        {
            id: 7,
            playerOne: "Нестеров Х. А.",
            playerTwo: "Исаева О. А.",
            winner: 1,
            date: "12 февраля 2022",
            time: "43 мин 13 сек",
        },
        {
            id: 8,
            playerOne: "Коновалова В. В.",
            playerTwo: "Терещенко У. Р.",
            winner: 2,
            date: "12 февраля 2022",
            time: "43 мин 13 сек",
        },{
            id: 9,
            playerOne: "Казаков Х. Е.",
            playerTwo: "Овчаренко Б. М.",
            winner: 2,
            date: "12 февраля 2022",
            time: "43 мин 13 сек",
        },
        {
            id: 10,
            playerOne: "Сасько Ц. А.",
            playerTwo: "Некифорова Б. А.",
            winner: 1,
            date: "12 февраля 2022",
            time: "43 мин 13 сек",
        },
    ]);
    return (
        <div className={classes.history}>
            <h3 className={classes.title}>История игр</h3>
            <Table history={history}/>
        </div>
    );
};

export default History;