import './App.css';
import {Routes, Route, Navigate} from 'react-router-dom';
import AuthLayout from "./layouts/AuthLayout";
import {publicRoutes, privateRoutes} from "./routes/index";
import GuestLayout from "./layouts/GuestLayout";

function App() {
    return (
        <div className="app">
            <Routes>
                <Route path="/" element={<Navigate to="/login"/> }></Route>
                <Route path="/" element={<AuthLayout/>}>
                    {privateRoutes.map((route) =>

                        <Route path={route.path} element={route.component} key={route.id}/>
                    )}
                </Route>
                <Route path="/" element={<GuestLayout/>}>
                    {publicRoutes.map((route) =>
                        <Route path={route.path} element={route.component} key={route.id}/>
                    )}
                </Route>
            </Routes>
        </div>
    );
}

export default App;
