import Login from "../pages/Auth/Login";
import Rating from "../pages/Rating/Rating";
import History from "../pages/History/History";
import Players from "../pages/Players/Players";
import ActivePlayers from "../pages/ActivePlayers/ActivePlayers";
import GameFieldPage from "../pages/GameField/GameFieldPage";

export const publicRoutes = [
    {
        id: 0, path: "/login", component: <Login/>
    },
];

export const privateRoutes = [
    {
        id: 2, path: "/rating", component: <Rating/>
    },
    {
        id: 3, path: "/history", component: <History/>
    },
    {
        id: 4, path: "/players", component: <Players/>
    },
    {
        id: 5, path: "/active-players", component: <ActivePlayers/>
    },
    {
        id: 6, path: "/game", component: <GameFieldPage/>
    }
];