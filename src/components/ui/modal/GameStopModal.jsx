import React from 'react';
import classes from "./game-stop.module.css";
import SubmitButton from "../button/SubmitButton";
import SecondaryButton from "../button/SecondaryButton";
import {useNavigate} from "react-router-dom";

const GameStopModal = ({children, resetGame}) => {
    const navigate = useNavigate();
    const setNewGame = () => {
        resetGame();
    }

    return (
        <div className={classes["modal-wpapper"]}>
            <div className={classes.modal}>
                <img src="/img/xxl-cup.svg" alt="cup"/>
                <h3>{children}</h3>
                <div className={classes["btn-group"]}>
                    <SubmitButton isDisabled={false} onClick={setNewGame}>Новая игра</SubmitButton>
                    <SecondaryButton onClick={() => {
                        navigate("/rating");
                    }}>Выйти в меню</SecondaryButton>
                </div>
            </div>
        </div>
    );
};

export default GameStopModal;