import React, {useMemo, useState} from 'react';
import classes from "./modal.module.css";
import Input from "../input/Input";
import GenderSelect from "../select/GenderSelect";
import CloseButton from "../button/CloseButton";
import SubmitButton from "../button/SubmitButton";


const Modal = ({setIsShow, storePlayer}) => {
    const [newPlayer, setNewPlayer] = useState({
        id: 0,
        fio: '',
        age: '',
        status: true,
        gender: false,
        dateStart: '',
        dateChange: '',
    });

    const selectGender = (e) => {
        const element = e.target.closest('input');
        if (element) {
            setNewPlayer({...newPlayer, gender: element.value});
        }
    }

    /* валидация полей, чтобы нельзя было вводить некорректные символы*/
    const validFIO = (e) => {
        const regex = /^[a-zA-ZА-Яа-я]|\s$|^$/;
        if (!regex.test(e.target.value)) {
            e.preventDefault();
        } else {
            setNewPlayer({...newPlayer, fio: e.target.value})
        }
    }

    const validAge = (e) => {
        const regex = /[0-9]|^$/;
        if (!regex.test(e.target.value)) {
            e.preventDefault();
        } else {
            setNewPlayer({...newPlayer, age: e.target.value})
        }
    }

    /* состояние isDisabled отвечает за показ или не показ кнопки, изменяется при изменении данных нового пользователя */
    let [isDisabled, setIsDisabled] = useState(false);
    useMemo(() => {
        setIsDisabled(newPlayer.fio.length > 0 && newPlayer.gender && newPlayer.age.length > 0);
    }, [newPlayer]);

    const getDate = () => {
        let today = new Date();

        const ms = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

        const d = String(today.getDate()).padStart(2, '0');
        const m = ms[today.getMonth()];
        const y = today.getFullYear();
        today = d + ' ' + m + ' ' + y;
        return today;
    }
    const createNewPlayer = (e) => {
        e.preventDefault();
        storePlayer({...newPlayer, id: Date.now(), status: true, dateStart: getDate(), dateChange: ''});
        setIsShow(false);
    }

    return (
        <div className={classes["modal-wrapper"]} onClick={() => {
            setIsShow(false)
        }}>
            <div className={classes.modal} onClick={(e) => {
                e.stopPropagation()
            }}>
                <div className={classes["close-btn-wrapper"]}>
                    <CloseButton onClick={() => {
                        setIsShow(false)
                    }}/>
                </div>
                <h3 className={classes.modal__header}>Добавьте игрока</h3>
                <form action="" method="post" className={classes.modal__form}>
                    <div className={classes["form-group"]}>
                        <label className={classes.label}>ФИО</label>
                        <Input
                            placeholder="Иванов Иван Иванович"
                            value={newPlayer.fio}
                            onChange={(e) => {
                                validFIO(e)
                            }}
                        />
                    </div>
                    <div className={classes["form-row"]}>
                        <div className={classes["form-group"]}>
                            <label className={classes.label}>Возраст</label>
                            <Input
                                placeholder="0"
                                style={{width: "72px"}}
                                value={newPlayer.age}
                                onChange={(e) => {
                                    validAge(e)
                                }}
                            />
                        </div>
                        <div className={classes["form-group"]}>
                            <label className={classes.label}>Пол</label>
                            <GenderSelect onClick={(e) => {
                                selectGender(e)
                            }}/>
                        </div>
                    </div>
                    <SubmitButton isDisabled={!isDisabled} disabled={!isDisabled} onClick={(e) => createNewPlayer(e)}>Добавить</SubmitButton>
                </form>
            </div>
        </div>
    );
};

export default Modal;