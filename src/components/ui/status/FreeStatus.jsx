import React from 'react';
import classes from "./free-status.module.css";

const FreeStatus = ({status}) => {
    return (
        <>
            {
                status
                ? <div className={`${classes.status} ${classes.free}`}>Свободен</div>
                : <div className={`${classes.status} ${classes.busy}`}>В игре</div>
            }
        </>
    );
};

export default FreeStatus;