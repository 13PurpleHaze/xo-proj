import React from 'react';
import classes from "./block-status.module.css";

const BlockStatus = ({isBlock}) => {
    return (
        <>
            {isBlock
                ? <div className={`${classes["block-status"]} ${classes.block}`}>Заблокирован</div>
                : <div className={`${classes["block-status"]} ${classes.free}`}>Активен</div>
            }
        </>
    );
};

export default BlockStatus;