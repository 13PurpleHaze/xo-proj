import React from 'react';
import classes from "./close-button.module.css";

const CloseButton = (props) => {
    return (
        <button className={classes.btn} onClick={props.onClick}><img className={classes.img} src="/img/close.svg" alt=""/></button>
    );
};

export default CloseButton;