import React from 'react';
import classes from "./general-button.module.css";

const GeneralButton = ({children, status, action, ...props}) => {
    return (
        <button className={status ? `${classes.btn} ${classes.active}` : `${classes.btn} ${classes.disabled}`} onClick={action} {...props}>{children}</button>
    );
};

export default GeneralButton;