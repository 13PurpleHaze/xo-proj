import React from 'react';
import classes from "./button-submit.module.css";
const SubmitButton = ({children, isDisabled, ...props}) => {
    return (
        <button className={isDisabled ? `${classes["btn-submit"]} ${classes.disabled}` : `${classes["btn-submit"]}` } {...props} onClick={props.onClick}>{children}</button>
    );
};

export default SubmitButton;