import React from 'react';
import classes from "./secondary-button.module.css";

const SecondaryButton = (props) => {
    return (
        <button className={classes.btn} onClick={props.onClick}>{props.children}</button>
    );
};

export default SecondaryButton;