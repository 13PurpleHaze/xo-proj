import React from 'react';
import classes from "./block-btn.module.css";
const BlockButton = ({action, isBlock}) => {
    return (
        <button className={classes["btn-block"]} onClick={action}>
            {
                !isBlock
                    ? <><img src="/img/block.svg" alt="" className={classes.img}/>{"Заблокировать"}</>
                : "Разблокировать"
            }
        </button>
    );
};

export default BlockButton;