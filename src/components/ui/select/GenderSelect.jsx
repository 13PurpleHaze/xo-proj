import React from 'react';
import classes from "./gender-select.module.css";

const GenderSelect = (props) => {
    return (
        <div className={classes.container} onClick={props.onClick}>
            <input type="radio" value="true" id="male" className={classes["input-hidden"]} name="gender"/>
            <label htmlFor="male" className={classes.label}>
                <img src="/img/male.svg" alt="male" className={classes["gender-img"]}/>
            </label>

            <input type="radio" id="female" value="false" className={classes["input-hidden"]} name="gender"/>
            <label htmlFor="female" className={classes.label}>
                <img src="/img/female.svg" alt="female" className={classes["gender-img"]}/>
            </label>
        </div>
    );
};

export default GenderSelect;