import React from 'react';
import classes from "./input.module.css";

const Input = ({error, ...props}) => {
    return (
        <input {...props}
               className={error ? `${classes.form__input} ${classes.invalid}` : `${classes.form__input}`}/>
    );
};

export default Input;