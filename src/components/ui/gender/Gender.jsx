import React from 'react';
import classes from "./gender.module.css";
const Gender = ({gender}) => {
    return (
        <>
            {gender
                ? <img src="/img/male.svg" alt="" className={classes.img}/>
                : <img src="/img/female.svg" alt="" className={classes.img}/>
            }
        </>
    );
};

export default Gender;