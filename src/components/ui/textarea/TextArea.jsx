import React from 'react';
import classes from "./textarea.module.css";

const TextArea = (props) => {
    return (
        <textarea {...props} className={classes.textarea}></textarea>
    );
};

export default TextArea;