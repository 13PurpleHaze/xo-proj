import React from 'react';
import "./toggle.css";

const Toggle = ({message, toggle, setToggle}) => {
    return (
        <div className="toggle">
            <div className="toggle__text">{message}</div>
            <input className="toggle__input" type="checkbox" id="toggle__input" onClick={(e) => {setToggle(!toggle)}}/>
            <label className="toggle__label" htmlFor="toggle__input"></label>
        </div>
    );
};

export default Toggle;