import React, {useEffect, useState} from 'react';
import classes from "../../pages/GameField/game-field.module.css";

const Timer = ({isOver, resetTimer, setResetTimer}) => {
    const [time, setTime] = useState({
        s:0,
        m:0,
        h:0,
    });
    useEffect(() => {
        if (resetTimer) {
            setResetTimer(false);
            return setTime({s:0, m:0, h:0});
        }
        if(isOver) {
            return () => clearInterval();
        }
        const calcTime = setInterval(() => {
            if (time.s === 59) {
                setTime({...time, s: 0, m: (time.m + 1)});
            } else {
                setTime({...time, s: (time.s + 1)});
            }
            if (time.m === 59) {
                setTime({...time, h: (time.h + 1), m: 0});
            }
        }, 1000);
        return () => clearInterval(calcTime)
    }, [time, resetTimer])
    return (
        <div className={classes["game-time"]}>{time.m.toString().padStart(2, '0')}:{time.s.toString().padStart(2, '0')}</div>
    );
};

export default Timer;