import React, { useState } from 'react';
import { NavLink } from "react-router-dom";
import "./navbar.css";

const NavBar = () => {
    const [showMenu, setShowMenu] = useState(false);
    return (
        <>
            <header className={showMenu ? "delete-shadow" : ""}>
                <div className="logo">
                    <img src={"/img/s-logo.svg"} alt=""/>
                </div>
                <nav className="nav">
                    <NavLink className="nav__link" to="/game">Игровое поле</NavLink>
                    <NavLink className="nav__link" to="/rating">Рейтинг</NavLink>
                    <NavLink className="nav__link" to="/active-players">Активные игроки</NavLink>
                    <NavLink className="nav__link" to="/history">История игр</NavLink>
                    <NavLink className="nav__link" to="/players">Список игроков</NavLink>
                </nav>
                <button className="header__btn">
                    <span className="logout-text">Выйти</span><img src={"/img/signout-icon.svg"} alt=""/>
                </button>
                <div className="hamburger" onClick={() => setShowMenu(!showMenu)}>
                    {showMenu ? <img src="/img/close.svg" alt="" /> : <img src="/img/burger.svg" alt="" />}
                </div>
            </header>  
           <div className={showMenu ? "drop-down-wrapper active" : "drop-down-wrapper"} onClick={() => setShowMenu(false)}>
                <div className={showMenu ? "drop-down-nav active" : "drop-down-nav"} onClick={(e) => e.stopPropagation()}>
                    <nav className="nav" onClick={() => {setShowMenu(false)}}>
                        <NavLink className="nav__link" to="/game">Игровое поле</NavLink>
                        <NavLink className="nav__link" to="/rating">Рейтинг</NavLink>
                        <NavLink className="nav__link" to="/active-players">Активные игроки</NavLink>
                        <NavLink className="nav__link" to="/history">История игр</NavLink>
                        <NavLink className="nav__link" to="/players">Список игроков</NavLink>
                    </nav>
                    <button className={showMenu ? "header__btn active" : "header__btn"}>
                        <span className="logout-text">Выйти</span><img src={"/img/signout-icon.svg"} alt=""/>
                    </button>
                </div>
            </div>
        </>
    );
};

export default NavBar;